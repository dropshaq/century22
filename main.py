########################################
# IMPORTS
########################################
import pygame
from pygame import mixer
from videos import show_start_video
########################################
# INIT
########################################
mixer.init()
pygame.init()
pygame.mixer.init()

music = pygame.mixer.music.load("audio/M1.mp3")
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.1)


if __name__ == "__main__":
    show_start_video()