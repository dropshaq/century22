########################################
# IMPORTS
########################################
import pygame
import os

from screen_vars import WIDTH, HEIGHT

########################################
# GAME BATTLE SCREEN VARIABLES
########################################
RED_SPACE_SHIP = pygame.image.load(os.path.join("graphics/pixel_ship_red_small.png"))
GREEN_SPACE_SHIP = pygame.image.load(os.path.join("graphics/pixel_ship_green_small.png"))
BLUE_SPACE_SHIP = pygame.image.load(os.path.join("graphics/pixel_ship_blue_small.png"))
YELLOW_SPACE_SHIP = pygame.image.load(os.path.join("graphics/pixel_ship_yellow.png"))
RED_LASER = pygame.image.load(os.path.join("graphics/pixel_laser_red.png"))
GREEN_LASER = pygame.image.load(os.path.join("graphics/pixel_laser_green.png"))
BLUE_LASER = pygame.image.load(os.path.join("graphics/pixel_laser_blue.png"))
YELLOW_LASER = pygame.image.load(os.path.join("graphics/pixel_laser_yellow.png"))
BG = pygame.transform.scale(pygame.image.load(os.path.join("graphics/background-black.png")), (WIDTH, HEIGHT))
