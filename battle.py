########################################
# IMPORTS
########################################
import pygame
import random
import sys

from videos import show_win_game_video

from menu import show_menu
from screen_vars import HEIGHT, win, WIDTH, SCREEN
from battle_vars import YELLOW_LASER, YELLOW_SPACE_SHIP, RED_LASER, RED_SPACE_SHIP, \
    BLUE_SPACE_SHIP, BLUE_LASER, GREEN_LASER, GREEN_SPACE_SHIP, BG


########################################
# BATTLE
########################################


def show_battle():
    while True:
        class Button:
            def __init__(self, image, pos, text_input, font, base_color, hovering_color):
                self.image = image
                self.x_pos = pos[0]
                self.y_pos = pos[1]
                self.font = font
                self.base_color, self.hovering_color = base_color, hovering_color
                self.text_input = text_input
                self.text = self.font.render(self.text_input, True, self.base_color)
                if self.image is None:
                    self.image = self.text
                self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))
                self.text_rect = self.text.get_rect(center=(self.x_pos, self.y_pos))

            def update(self, screen):
                if self.image is not None:
                    screen.blit(self.image, self.rect)
                screen.blit(self.text, self.text_rect)

            def changeColor(self, position):
                if position[0] in range(self.rect.left, self.rect.right) and position[1] in range(self.rect.top,
                                                                                                  self.rect.bottom):
                    self.text = self.font.render(self.text_input, True, self.hovering_color)
                else:
                    self.text = self.font.render(self.text_input, True, self.base_color)

            def draw(self, surface):
                action = False

                # get mouse position
                pos = pygame.mouse.get_pos()

                # check mouseover and clicked conditions
                if self.rect.collidepoint(pos):
                    if pygame.mouse.get_pressed()[0] == 1 and self.clicked == False:
                        action = True
                        self.clicked = True

                if pygame.mouse.get_pressed()[0] == 0:
                    self.clicked = False

                # draw button
                surface.blit(self.image, (self.rect.x, self.rect.y))

                return action

        def get_font(size):  # Returns Press-Start-2P in the desired size
            return pygame.font.Font("assets/font.ttf", size)

        class Laser:
            def __init__(self, x, y, img):
                self.x = x
                self.y = y
                self.img = img
                self.mask = pygame.mask.from_surface(self.img)

            def draw(self, window):
                window.blit(self.img, (self.x, self.y))

            def move(self, vel):
                self.y += vel

            def off_screen(self, height):
                return not (self.y <= height and self.y >= 0)

            def collision(self, obj):
                return collide(self, obj)

        class Ship:
            COOLDOWN = 30

            def __init__(self, x, y, health=100):
                self.x = x
                self.y = y
                self.health = health
                self.ship_img = None
                self.laser_img = None
                self.lasers = []
                self.cool_down_counter = 0

            def draw(self, window):
                window.blit(self.ship_img, (self.x, self.y))
                for laser in self.lasers:
                    laser.draw(window)

            def move_lasers(self, vel, obj):
                self.cooldown()
                for laser in self.lasers:
                    laser.move(vel)
                    if laser.off_screen(HEIGHT):
                        self.lasers.remove(laser)
                    elif laser.collision(obj):
                        obj.health -= 10
                        self.lasers.remove(laser)

            def cooldown(self):
                if self.cool_down_counter >= self.COOLDOWN:
                    self.cool_down_counter = 0
                elif self.cool_down_counter > 0:
                    self.cool_down_counter += 1

            def shoot(self):
                if self.cool_down_counter == 0:
                    laser = Laser(self.x, self.y, self.laser_img)
                    self.lasers.append(laser)
                    self.cool_down_counter = 1

            def get_width(self):
                return self.ship_img.get_width()

            def get_height(self):
                return self.ship_img.get_height()

        class Player(Ship):
            def __init__(self, x, y, health=100):
                super().__init__(x, y, health)
                self.ship_img = YELLOW_SPACE_SHIP
                self.laser_img = YELLOW_LASER
                self.mask = pygame.mask.from_surface(self.ship_img)
                self.max_health = health

            def move_lasers(self, vel, objs):
                self.cooldown()
                for laser in self.lasers:
                    laser.move(vel)
                    if laser.off_screen(HEIGHT):
                        self.lasers.remove(laser)
                    else:
                        for obj in objs:
                            if laser.collision(obj):
                                objs.remove(obj)
                                if laser in self.lasers:
                                    self.lasers.remove(laser)

            def draw(self, window):
                super().draw(window)
                self.healthbar(window)

            def healthbar(self, window):
                pygame.draw.rect(window, (255, 0, 0),
                                 (self.x, self.y + self.ship_img.get_height() + 10, self.ship_img.get_width(), 10))
                pygame.draw.rect(window, (0, 255, 0), (self.x, self.y + self.ship_img.get_height() + 10,
                                                       self.ship_img.get_width() * (self.health / self.max_health), 10))

        class Enemy(Ship):
            COLOR_MAP = {
                "red": (RED_SPACE_SHIP, RED_LASER),
                "green": (GREEN_SPACE_SHIP, GREEN_LASER),
                "blue": (BLUE_SPACE_SHIP, BLUE_LASER)
            }

            def __init__(self, x, y, color, health=100):
                super().__init__(x, y, health)
                self.ship_img, self.laser_img = self.COLOR_MAP[color]
                self.mask = pygame.mask.from_surface(self.ship_img)

            def move(self, vel):
                self.y += vel

            def shoot(self):
                if self.cool_down_counter == 0:
                    laser = Laser(self.x - 20, self.y, self.laser_img)
                    self.lasers.append(laser)
                    self.cool_down_counter = 1

        def collide(obj1, obj2):
            offset_x = obj2.x - obj1.x
            offset_y = obj2.y - obj1.y
            return obj1.mask.overlap(obj2.mask, (offset_x, offset_y)) != None

        def main():
            run = True
            FPS = 60
            level = 0
            lives = 3
            main_font = pygame.font.SysFont("comicsans", 50)
            lost_font = pygame.font.SysFont("comicsans", 60)

            enemies = []
            wave_length = 5
            enemy_vel = 1

            player_vel = 5
            laser_vel = 5

            player = Player(300, 630)

            clock = pygame.time.Clock()

            lost = False
            lost_count = 0

            def redraw_window():
                win.blit(BG, (0, 0))
                # draw text
                lives_label = main_font.render(f"Lives: {lives}", 1, (255, 255, 255))
                level_label = main_font.render(f"Level: {level}", 1, (255, 255, 255))

                win.blit(lives_label, (10, 10))
                win.blit(level_label, (WIDTH - level_label.get_width() - 10, 10))

                for enemy in enemies:
                    enemy.draw(win)

                player.draw(win)

                if lost:
                    lost_label = lost_font.render("You Died!! Restarting...", 1, (255, 255, 255))
                    win.blit(lost_label, (WIDTH / 2 - lost_label.get_width() / 2, 350))

                pygame.display.update()

            while run:
                clock.tick(FPS)
                redraw_window()

                if lives <= 0 or player.health <= 0:
                    lost = True
                    lost_count += 1

                if lost:
                    if lost_count > FPS * 3:
                        run = False
                        show_menu()
                    else:
                        continue

                if len(enemies) == 0:
                    level += 1
                    wave_length += 5
                    for i in range(wave_length):
                        enemy = Enemy(random.randrange(50, WIDTH - 100), random.randrange(-1500, -100),
                                      random.choice(["red", "blue", "green"]))
                        enemies.append(enemy)

                    if level == 3:
                        show_win_game_video()

                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        quit()
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        if event.button == 1:
                            player.shoot()

                keys = pygame.key.get_pressed()
                if keys[pygame.K_a] and player.x - player_vel > 0:  # left
                    player.x -= player_vel
                if keys[pygame.K_d] and player.x + player_vel + player.get_width() < WIDTH:  # right
                    player.x += player_vel
                if keys[pygame.K_w] and player.y - player_vel > 0:  # up
                    player.y -= player_vel
                if keys[pygame.K_s] and player.y + player_vel + player.get_height() + 15 < HEIGHT:  # down
                    player.y += player_vel

                for enemy in enemies[:]:
                    enemy.move(enemy_vel)
                    enemy.move_lasers(laser_vel, player)

                    if random.randrange(0, 2 * 60) == 1:
                        enemy.shoot()

                    if collide(enemy, player):
                        player.health -= 10
                        enemies.remove(enemy)
                    elif enemy.y + enemy.get_height() > HEIGHT:
                        lives -= 1
                        enemies.remove(enemy)
                    pygame.display.update()

                if level == 2:
                    MENU_MOUSE_POS = pygame.mouse.get_pos()

                    RUN_BUTTON = Button(
                        image=pygame.image.load("assets/Options Rect.png"), pos=(640, 400),
                        text_input="OPTIONS", font=get_font(75), base_color="#d7fcd4", hovering_color="#ddff11"
                    )

                    for button in [RUN_BUTTON]:
                        button.changeColor(MENU_MOUSE_POS)
                        button.update(SCREEN)

                # for event in pygame.event.get():
                # if RUN_BUTTON.checkForInput(MENU_MOUSE_POS):
                # RUN()
                player.move_lasers(-laser_vel, enemies)

        def main_menu():
            title_font = pygame.font.SysFont("comicsans", 70)
            run1 = True
            # DOES NOT RESTART HERE
            while run1:
                win.blit(BG, (0, 0))
                title_label = title_font.render("Press the mouse to begin...", 1, (255, 255, 255))
                win.blit(title_label, (WIDTH / 2 - title_label.get_width() / 2, 350))
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        run1 = False
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        main()
            pygame.quit()

        main_menu()
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

show_battle()