import pygame
import os
from pygame import mixer

from screen_vars import win_width, win_height
pygame.mixer.init()
########## OUTSIDE VARIABLES##########
back_1 = [
    pygame.image.load(os.path.join('Blake', 'BACK.png')),
    pygame.image.load(os.path.join('Blake', 'BACK1.png'))
]
stationary = [
    pygame.image.load(os.path.join('Blake', 'STATIONARY.png')),
    pygame.image.load(os.path.join('Blake', 'STATIONARY1.png'))
]
left = [
    pygame.image.load(os.path.join('Blake', 'L2.png')),
    pygame.image.load(os.path.join('Blake', 'L1.png'))
]
right = [
    pygame.image.load(os.path.join('Blake', 'R2.png')),
    pygame.image.load(os.path.join('Blake', 'R1.png'))
]

bg = pygame.transform.scale(pygame.image.load(os.path.join("img/BG1.jpg")), (win_width, win_height))
music = pygame.mixer.music.load("audio/M1.mp3")
laser_fx = pygame.mixer.Sound("audio/LAZER.mp3")
explosion_fx = pygame.mixer.Sound("audio/Explosion.mp3")
explosion2_fx = pygame.mixer.Sound("audio/Explosion.mp3")
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.1)