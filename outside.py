########################################
# IMPORTS
########################################
import pygame

from screen_vars import win_width, win
from outside_vars import left, right, back_1, stationary, bg

#pygame.mixer.init()
########################################
# OUTSIDE
########################################
def show_outside():
	while True:
		class Hero:
			def __init__(self, x, y):
				# WALK
				self.x = x
				self.y = y
				self.velx = 10
				self.vely = 10
				self.face_right = False
				self.face_left = False
				self.face_up = False
				self.face_down = False
				self.stepIndex = 0
				# Jump
				self.jump = False

			def move_hero(self, userInput):
				if userInput[pygame.K_d] and self.x <= win_width - 62:
					self.x += self.velx
					self.face_right = True
					self.face_left = False
					self.face_up = False
					self.face_down = False
				elif userInput[pygame.K_a] and self.x >= 0:
					self.x -= self.velx
					self.face_right = False
					self.face_left = True
					self.face_up = False
					self.face_down = False
				elif userInput[pygame.K_w]:
					self.face_right = False
					self.face_left = False
					self.face_up = True
					self.face_down = False
				elif userInput[pygame.K_s]:
					self.face_right = False
					self.face_left = False
					self.face_up = False
					self.face_down = True
				else:
					self.stepIndex = 0

			def draw(self, win):
				if self.stepIndex >= 2:
					self.stepIndex = 0
				if self.face_left:
					win.blit(left[self.stepIndex], (self.x, self.y))
					self.stepIndex += 1
				elif self.face_right:
					win.blit(right[self.stepIndex], (self.x, self.y))
					self.stepIndex += 1
				elif self.face_up:
					win.blit(back_1[self.stepIndex], (self.x, self.y))
				elif self.face_down:
					win.blit(stationary[self.stepIndex], (self.x, self.y))
				else:
					win.blit(stationary[self.stepIndex], (self.x, self.y))

			def jump_motion(self, userInput):
				if userInput[pygame.K_SPACE] and self.jump is False:
					self.jump = True
				if self.jump:
					self.y -= self.vely * 4
					self.vely -= 1
				if self.vely < -10:
					self.jump = False
					self.vely = 10


		########## DRAW GAME ##########
		def draw_game():
			global stepIndex
			win.fill((0, 0, 0))
			win.blit(bg, (0, 0))
			player.draw(win)
			pygame.time.delay(100)
			pygame.display.update()

		########## HERO INSTANCE ##########
		player = Hero(100, 580)

		#################### MAIN ####################
		run = True
		while run:
			# Quit Game
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					run = False
			# Input
			userInput = pygame.key.get_pressed()

			# Movement
			player.move_hero(userInput)
			player.jump_motion(userInput)

			# Draw Game In Window
			draw_game()
show_outside()