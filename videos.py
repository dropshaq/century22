########################################
# IMPORTS
########################################
from moviepy.editor import VideoFileClip
import pygame
import os


from screen_vars import SCREEN_WIDTH, SCREEN_HEIGHT

########################################
# VIDEOS INIT
########################################
def show_start_video():
    clip = VideoFileClip('./video/meh.mp4')
    clip.size=(SCREEN_WIDTH, SCREEN_HEIGHT)
    clip.show(interactive=False)
    clip.preview()
    clip.close()
    from menu import show_menu
    show_menu()

def show_going_to_space_video():
    clip = VideoFileClip('./video/meh.mp4')
    clip.size=(SCREEN_WIDTH, SCREEN_HEIGHT)
    clip.preview()
    clip.close()
    from battle import show_battle
    show_battle()


def show_win_game_video():
    clip = VideoFileClip('./video/meh.mp4')
    clip.size=(SCREEN_WIDTH, SCREEN_HEIGHT)
    clip.preview()
    clip.close()
