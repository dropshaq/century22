########################################
# IMPORTS
########################################
import pygame

########################################
# INIT
########################################
pygame.font.init()

########################################
# SCREEN VARIABLES
########################################
win_height = 800
win_width = 1400
SCREEN_WIDTH = 1400
SCREEN_HEIGHT = 800
screen = pygame.display.set_mode((1400, 800))
pygame.display.set_caption("CENTURY 22")
WIDTH, HEIGHT = 1400, 800
win = pygame.display.set_mode((WIDTH, HEIGHT))
SCREEN = pygame.display.set_mode((1400, 800))
BG = pygame.image.load("assets/Background.png")
clock = pygame.time.Clock()
FPS = 60
CLOCK = pygame.time.Clock()
black = (0, 0, 0)
FONT = pygame.font.SysFont("Roboto", 100)
